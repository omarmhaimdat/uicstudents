# [UIC-STUDENTS](http://uic-students.ml)
Un site fait par des étudiants pour les étudiants de l'Université Internationale de Casablanca, afin de les aider durant leurs années d'études à l'UIC.

## Le but du site :

* facilité l’accessibilité des supports de cours aux étudiants.
* Lancement de système d’entraide entre les étudiants.
* Motiver les étudiants à produire et réaliser des projets.

## Motivation derrière le site:
J’ai écris un article qui explique assez bien ma démarche:
[Les 1 Mois du Site](http://uic-students.ml/les_un_mois_du_site.html)

## Initiateur:
Omar M’Haimdat.

## Contributeurs
* Omar M’HAIMDAT
* Nouamane MAJD
* Anas SORY
* Zainou BARA
* Hafsa HMAMA
* Khalid TALAAT IDRISSI
* Abd El Ahad BOURASS

## License
This project is licensed under the MIT License - see the [LICENSE.md](https://github.com/omarmhaimdat/uicstudents/blob/master/LICENSE) file for details
